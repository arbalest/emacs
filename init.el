;;; package --- Summary
;;; Commentary:
;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;; (package-initialize)

;; This loads the actual configuration in literate org-mode elisp
(defun load-config()
  "Load the configuration file."
  (interactive)
  (setq config-dir (file-name-directory (or (buffer-file-name) load-file-name)))
  (org-babel-load-file (expand-file-name "configuration.org" config-dir))
  )

(load-config)
(message (concat "Emacs started in " (emacs-init-time)))
(provide '.emacs)
;;;  .emacs ends here
